Nice Micro Backup utility
=========================

Nice Micro Backup utility is a BASH script developed mainly for Linux computers for creating incremental system backups using rsync, similarly to Apple's Time Machine or teejee2008's Timeshift.

Main purpose
------------

The utility makes a copy of the source directory (set up in the SRC variable) to the backup directory (BCKP variable), under a subdirectory based on whether the script was invoked with or without command-line parameters. There is also an array of excluded paths (variable EXCLUDE).
